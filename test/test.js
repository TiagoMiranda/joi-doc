/**
 * Created by Alexander Nuikin (nukisman@gmail.com) on 10.06.15.
 */
'use strict';

var Joi = require('joi')
var JoiDoc = require('../')
var StackEdit = require('../format/md/stackedit')

var schemaFunc = Joi.func().description('My Function').meta({
    args: [
        Joi.number().description('Count'),
        Joi.string().description('Name')
    ],
    return: Joi.string().description('Names of smth')
})

var schema = {
    age: Joi.number().integer().required().description('My number').min(18).max(99).default(25).unit('годы'),
    gender: Joi.string().optional().valid('male', 'female').description('User\'s \ngender   '),
    geo: Joi.object().optional().description('Площадь покрытия').keys({
        lat: Joi.number().required().description('Широта'),
        lon: Joi.number().required().description('Долгота'),
        radius: Joi.number().integer().optional().description('Радиус').default(100).unit('метры')
    })
        .unknown(false)
        .example({
            lat: 123,
            lon: 456.789
        })
        .example({
            lat: 987,
            lon: 6456.789,
            radius: 333
        }),
    list: Joi.array().forbidden().items(
        Joi.string().description('Key').allow(''),
        Joi.number().description('Value'),
        schemaFunc
    ).length(3)
}

var rows = []
for (var key in schema) {
    JoiDoc.visit(schema[key], rows, [key])
}
console.log('\n', StackEdit.format(rows))


schema = Joi.object().keys(schema)
rows = []
JoiDoc.visit(schema, rows, [])
console.log(StackEdit.format(rows))


console.log('\n', StackEdit.formatJoiFunc(schemaFunc))


var schemaFuncSingleArg = Joi.func().description('My Single Arg Function').meta({
    args: [
        Joi.object().keys({
            count: Joi.number().description('Count'),
            name: Joi.string().description('Name')
        })
            .example({
                count: 434,
                name: 'Vasya Pupkin'
            })
            .example({
                count: 999,
                name: 'Alexander Nuikin'
            })
    ],
    return: Joi.object().keys({
        x: Joi.number(),
        y: Joi.number()
    })
        .example({
            x: 1,
            y: 2
        })
        .example({
            x: 3,
            y: 4
        })
})
console.log('\n', StackEdit.formatJoiFunc(schemaFuncSingleArg, true))